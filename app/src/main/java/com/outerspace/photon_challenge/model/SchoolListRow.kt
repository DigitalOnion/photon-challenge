package com.outerspace.photon_challenge.model


class SchoolListRow (
    @com.squareup.moshi.Json(name = "dbn")
    val dbn: String,
    @com.squareup.moshi.Json(name = "school_name")
    val schoolName: String,
    @com.squareup.moshi.Json(name = "neighborhood")
    val neighborhood: String,
    @com.squareup.moshi.Json(name = "city")
    val city: String,
    )
