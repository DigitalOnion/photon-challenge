package com.outerspace.photon_challenge.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintSet.Constraint
import androidx.lifecycle.lifecycleScope
import com.outerspace.photon_challenge.R
import com.outerspace.photon_challenge.model.SchoolApi

private const val DBN_PARAM = "dbnParam"

class DetailFragment : Fragment() {
    private var dbnParam: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            dbnParam = it.getString(DBN_PARAM)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val detailFragmentView =  inflater.inflate(R.layout.fragment_detail, container, false)
        displayDetail(detailFragmentView, dbnParam)
        return detailFragmentView
    }

    companion object {
        fun newInstance(dbmParam: String) = DetailFragment().apply {
            arguments = Bundle().apply {
                putString(DBN_PARAM, dbmParam)
            }
        }
    }

    private fun displayDetail(view: View, dbnParam: String?) {
        lifecycleScope.launchWhenStarted {
            val satScores = SchoolApi.getSatScores(dbnParam!!)
            if(satScores.isNotEmpty()) {
                view.findViewById<ViewGroup>(R.id.valid_data_layout).visibility = View.VISIBLE
                view.findViewById<ViewGroup>(R.id.invalid_data_layout).visibility = View.GONE
                with(satScores[0]) {
                    view.findViewById<TextView>(R.id.detail_school_name).text = this.schoolName
                    view.findViewById<TextView>(R.id.critical_reading_avg_score).text = this.satCriticalReadingAvgScore
                    view.findViewById<TextView>(R.id.math_avg_score).text = this.satMathAvgScore
                    view.findViewById<TextView>(R.id.writing_avg_score).text = this.satWritingAvgScore
                    view.findViewById<TextView>(R.id.detail_dbn).text = this.dbn
                }
            } else {
                view.findViewById<ViewGroup>(R.id.valid_data_layout).visibility = View.GONE
                view.findViewById<ViewGroup>(R.id.invalid_data_layout).visibility = View.VISIBLE
                view.findViewById<TextView>(R.id.invalid_dbn).text = getString(R.string.invalid_data_notice, dbnParam)

            }

        }


    }

}