package com.outerspace.photon_challenge.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.outerspace.photon_challenge.R
import com.outerspace.photon_challenge.model.SchoolListRow

class MainRecyclerAdapter(private val parentActivity: FragmentActivity, private val dataSet: List<SchoolListRow>):
    RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder>() {
        class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
            val schoolNameView: TextView
            val neighbourhoodView: TextView
            val schoolRecord: View
            init {
                schoolNameView = view.findViewById(R.id.school_name)
                neighbourhoodView = view.findViewById(R.id.city_neighborhood)
                schoolRecord = view.findViewById(R.id.school_record)
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.main_view_holder, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(dataSet[position]) {
            holder.schoolNameView.text = schoolName
            holder.neighbourhoodView.text = holder.itemView.context
                .getString(R.string.neighbourhood_city, neighborhood, city)

            holder.schoolRecord.setOnClickListener {
                val detailFragmentInstance = DetailFragment.newInstance(dbn)
                this@MainRecyclerAdapter.parentActivity.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, detailFragmentInstance)
                    .addToBackStack(null)
                    .commit()
            }
        }
    }

    override fun getItemCount() = dataSet.size
}