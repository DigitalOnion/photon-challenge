# Luis Viruena - Coding Challenge for Photon

This repository was prepared for the Photon challenge, in a way that I don't take time to worry about setting up a new project in GitLab.

The project contains:
- An Empty Activity
- Gradle dependencies for:
-- Retrofit and Moshi
-- Kotlin support for Coroutines

Luis.
